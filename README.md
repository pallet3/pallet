# pallet



## Installation

To install pallet just download `pallet.sh` and run it in the terminal by executeing `./pallet.sh`. You can put the file in any folder.

## Useing pallet

When Ran, pallet will display a message telling you to type in a colour. You can enter it in all caps, lowwercase, or realy any way. Possible colours are blue, green, yellow, orange, red, purple, brown, white and black. From then, unless black or white was selected, it will tell you to input a shade, 1 to 5, 1 to 8, or 1 to 4. The higher the number the darker the shade. An example is shown here:
```none
    Please input a colour. it will be outputted in hex and rgb
    Blue
    Please chose a shade, 1-5
    1
    #99c1f1 (153, 193, 241)
```

## Roadmap

Here is our current Roadmap, and what we will add in the future
1. a testing branch
2. Command line arguments (ex. `./pallet.sh --blue -2`)
3. More pallet options (ex. solarized-light, solarized-dark, tango, tango-dark, breeze, breeze-dark)
4. Add it to the AUR
5. A relese on to a debian/fedora repo

## More info

This program is not lisensed. This program uses a colour pallet from the GNOME HIG
